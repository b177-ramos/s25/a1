// Activity Instructions:
// 1. Create an a1 folder and inside create an activity.js file to write and save the solution for the activity.

// 2. Use the count operator to count the total number of fruits on sale.

// 3. Use the count operator to count the total number of fruits with stock more than 20.

// 4. Use the average operator to get the average price of fruits onSale per supplier.

// 5. Use the max operator to get the highest price of a fruit per supplier.

// 6. Use the min operator to get the lowest price of a fruit per supplier.

// 7. Create a gitlab project repository named a1 in s25.

// 8. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.

// 9. Add the link in Boodle.

// Use the count operator to count the total number of fruits on sale.
db.fruits.aggregate([
	{
      $match : {onSale : true}
    },
	{
      $count : "fruits"
    }
]);


// Use the count operator to count the total number of fruits with stock more than 20.
db.fruits.aggregate([
	{
		$match : { stock : { $gt : 20 } }
	},

	{
		$count : "name"
	}
]);


// Use the average operator to get the average price of fruits onSale per supplier.
db.fruits.aggregate([
	{
		$match : { onSale : true }
	},
	{
		$group : { _id : "$supplier_id", total : { $avg: "$price" } }
	}
]);


// Use the max operator to get the lowest price of a fruit per supplier.
db.fruits.aggregate([
	{
		$group : { _id : "$supplier_id", total : { $max : "$price" } }
	}
]);


// Use the min operator to get the lowest price of a fruit per supplier.
db.fruits.aggregate([
	{
		$group : { _id : "$supplier_id", total : { $min : "$price" } }
	}
]);
